#!/bin/bash
# Helper script to start bot from init script

WDIR=/home/jukebox_bot/jukebox_bot
VIRTUALENV_DIR=/home/jukebox_bot/.virtualenvs/jukebox_bot

source $VIRTUALENV_DIR/bin/activate
source $VIRTUALENV_DIR/bin/postactivate

cd $WDIR

./jukebox_bot.py
