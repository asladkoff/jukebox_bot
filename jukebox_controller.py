import json
import logging
import requests
import time


class JukeboxController(object):

    def __init__(self):
        # TODO: what is id? should I increment it with each request?
        self.data = {"jsonrpc": "2.0", "id": 1}
        self.headers = {'Content-Type': 'application/json'}
        self.url = 'http://localhost:6680/mopidy/rpc'

    def _send_command(self, action, data=None, headers=None):
        _data = self.data.copy()
        _headers = self.headers.copy()
        if data:
            _data.update(data)
        if headers:
            _headers.update(headers)
        logging.debug('Performing %s with json=%s, headers=%s', action, _data, _headers)
        response = requests.post('http://localhost:6680/mopidy/rpc', json=_data, headers=_headers)

        error = json.loads(response.text).get('error')
        if error:
            logging.error(json.loads(response.text).get('error'))
            return False

        logging.debug("%s result: %s", action, json.loads(response.text).get('result', False))
        return json.loads(response.text).get('result', False)

    def _add_playlist_or_track(self, params):
        for playlist in self._playlists_as_list():
            if playlist.get('name') == params:
                print("found"*30)
                uris = []
                for track in self._playlists_get_items(playlist.get('uri')):
                    uris.append(track.get('uri'))
                self._tracklist_add(uris=uris)

    def _get_current_track(self):
        result = self._send_command(action='get_current_track',
                                    data={"method": "core.playback.get_current_tl_track", "params": {}})
        if result:
            track = result.get('track', {})
            title = track.get('name', 'Unknown track')
            try:
                artist = track.get('artists', [])[0].get('name', 'Unknown artist')
            except IndexError:
                artist = "Unknown artist"
            return '"{}" by "{}"'.format(title, artist)

    def _get_state(self):
        result = self._send_command(action='get_state',
                                    data={"method": "core.playback.get_state", "params": {}})
        return result or ''

    def _get_mute(self):
        result = self._send_command(action='get_mute',
                                    data={"method": "core.playback.get_mute", "params": {}})
        return result

    def _playlists_as_list(self):
        result = self._send_command(action='playlists.as_list',
                                    data={'method': 'core.playlists.as_list', 'params': {}})
        return result or []

    def _playlists_get_items(self, playlist_uri):
        result = self._send_command(action='playlists.get_items',
                                    data={'method': 'core.playlists.get_items', 'params': {'uri': playlist_uri}})
        return result or []

    def _tracklist_add(self, uris=None):
        if uris:
            result = self._send_command(action='tracklist.add',
                                        data={'method': 'core.tracklist.add', 'params': {'uris': uris}})
        return result

    def info(self):
        result = []

        current_track = self._get_current_track()
        if current_track:
            result.append("Current track: {}".format(current_track))

        result.append('Muted: {}'.format(self._get_mute()))
        result.append('State: {}'.format(self._get_state()))
        result.append(self.volume())

        return '\n'.join(result)

    def mute(self):
        if self._send_command(action='mute', data={"method": "core.playback.set_mute", "params": {"mute": True}}):
            return 'Muted'

    def pause(self):
        if self._send_command(action='pause', data={"method": "core.playback.pause", "params": {}}) is None:
            return 'Paused'     # pause returns null

    def play(self, params=''):
        if params:
            self._add_playlist_or_track(params)

        self._send_command(action='play', data={"method": "core.playback.play", "params": {}})
        time.sleep(1)
        if self._get_state() == 'playing':
            return 'Playing {}'.format(self._get_current_track())
        else:
            return 'No track to play'

    def resume(self):
        current_track = self._get_current_track()
        if current_track:
            if self._send_command(action='resume', data={"method": "core.playback.resume", "params": {}}) is None:
                return 'Resumed playing {}'.format(self._get_current_track())   # pause returns null
        else:
            return 'No track to resume'

    def stop(self):
        if self._send_command(action='stop', data={"method": "core.playback.stop", "params": {}}) is None:
            return 'Stopped'   # stop returns null

    def unmute(self):
        if self._send_command(action='unmute', data={"method": "core.playback.set_mute", "params": {"mute": False}}):
            return 'Unmuted'

    def volume(self, params=''):
        if params:
            try:
                volume = int(params.split(' ')[0])
                if volume not in range(0, 101):
                    raise ValueError
            except ValueError:
                return 'Invalid volume value, should be number from 0 to 100.'
            if self._send_command(action='set_volume',
                                  data={"method": "core.playback.set_volume", "params": {"volume": volume}}):
                return 'Volume set to {volume}'.format(volume=volume)
        else:
            volume = self._send_command(action='get_volume', data={"method": "core.playback.get_volume"})
            if volume:
                return 'Volume: {volume}'.format(volume=volume)
