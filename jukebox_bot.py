#!/usr/bin/env python

import argparse
import logging
import os
import sys
import time

from emoji import emojize
from pyngrok import ngrok
from telegram.ext import CommandHandler, Updater
try:
    from urllib.request import urlopen, URLError
except ImportError:
    from urllib2 import urlopen, URLError

from jukebox_controller import JukeboxController


class Bot(object):
    COMMANDS = [
        ['help', 'Print help message'],
        ['info', 'Print track info'],
        ['mute', 'Mute sound'],
        ['unmute', 'Unmute sound'],
        ['pause', 'Pause playback'],
        ['resume', 'Resume playback'],
        ['play', 'Play track'],
        ['stop', 'Stop playback'],
        ['volume', 'Set volume to value from 0 to 100 or get current value'],
    ]

    def __init__(self):
        self.port = self._get_env(name='JUKEBOX_BOT_WEBHOOK_PORT', default=5000, convert_to_type=int)
        self.ngrok_token = self._get_env('JUKEBOX_BOT_NGROK_TOKEN', compulsory=True)
        self.token = self._get_env('JUKEBOX_BOT_TELEGRAM_TOKEN', compulsory=True)

        ngrok.set_auth_token(self.ngrok_token)
        url = ngrok.connect(port=self.port, proto='http', options={'bind_tls': True})
        url = url.replace('http://', 'https://')    # strange bug in pyngrok: connect returns http instead of https
        url = "{}/{}".format(url, self.token)

        self.updater = Updater(token=self.token)
        self.updater.start_webhook(listen="0.0.0.0", port=self.port, url_path=self.token)
        self.updater.bot.setWebhook(url)

        self.dispatcher = self.updater.dispatcher
        self.jukebox_controller = JukeboxController()
        for command, _ in self.COMMANDS:
            self.dispatcher.add_handler(CommandHandler(command, self.__getattribute__(command), allow_edited=True))

    def _do_action(self, action, bot, update, extract_parameters=False):
        message = update.message if update.message else update.edited_message
        logging.debug('%s by %s %s', action, message.from_user.first_name, message.from_user.last_name)

        action_handler = self.jukebox_controller.__getattribute__(action)
        if extract_parameters:
            params = ' '.join(message.text.split(' ')[1:])
            result = action_handler(params=params)
        else:
            result = action_handler()

        if result:
            message_id = None if update.message else message.message_id
            bot.send_message(chat_id=message.chat_id, text=result, reply_to_message_id=message_id)

    @staticmethod
    def _get_env(name=None, compulsory=False, default=None, convert_to_type=str):
        env_var = os.getenv(name)
        if env_var is None:
            if compulsory:
                logging.error('Please set environment variable %s', name)
                exit(1)
            env_var = default

        try:
            return convert_to_type(env_var)
        except ValueError:
            logging.error('Cannot convert %s value "%s" to %s', name, env_var, convert_to_type)
            exit(1)

    def help(self, bot, update):
        icon = emojize(":information_source: ", use_aliases=True)
        text = icon + " The following commands are available:\n"
        text += '\n'.join(['/{} {}'.format(command, description) for command, description in self.COMMANDS])
        bot.send_message(chat_id=update.message.chat_id, text=text)

    def info(self, bot, update):
        self._do_action('info', bot, update)

    def mute(self, bot, update):
        self._do_action('mute', bot, update)

    def pause(self, bot, update):
        self._do_action('pause', bot, update)

    def resume(self, bot, update):
        self._do_action('resume', bot, update)

    def play(self, bot, update):
        self._do_action('play', bot, update, extract_parameters=True)

    def stop(self, bot, update):
        self._do_action('stop', bot, update)

    def unmute(self, bot, update):
        self._do_action('unmute', bot, update)

    def volume(self, bot, update):
        self._do_action('volume', bot, update, extract_parameters=True)

    def run(self):
        self.updater.idle()


def configure_logger(debug=False):
    logging_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logger = logging.getLogger()
    logger.setLevel(level=logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(logging.Formatter(logging_format))
    level = logging.DEBUG if debug else logging.ERROR
    ch.setLevel(level)
    logger.addHandler(ch)

    fh = logging.FileHandler('jukebox_bot.log', mode='a')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter(logging_format))
    logger.addHandler(fh)


def check_connectivity(reference):
    try:
        urlopen(reference, timeout=1)
        return True
    except URLError:
        logging.debug('URLError:', exc_info=True)
        return False


def wait_for_internet():
    while not check_connectivity("https://api.telegram.org"):
        logging.error("Waiting for internet")
        time.sleep(5)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Telegram bot to control Mopidy")
    parser.add_argument('-d', '--debug', action='store_true', help='Turn on debug mode')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    configure_logger(args.debug)

    wait_for_internet()
    Bot().run()
